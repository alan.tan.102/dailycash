package dccash.money.$security.config;

import java.security.SecureRandom;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;


@Configuration
@Order(1)
//@ComponentScan(basePackageClasses = CustomUserDetailsService.class)
public class WebSecurityConfig_admin extends WebSecurityConfigurerAdapter {

// @Autowired 
// private UserDetailsService userDetailsService;
// 
// @Autowired
// public void configAuthentication(AuthenticationManagerBuilder auth) throws Exception {    
//	 auth.userDetailsService(userDetailsService).passwordEncoder(passwordencoder());
// } 

 @Override
 protected void configure(HttpSecurity http) throws Exception {
	 
	   http.csrf().disable(); 
	   http.authorizeRequests().anyRequest().permitAll(); 
	  
 }
 
 @Bean(name="passwordEncoder")
    public PasswordEncoder passwordencoder(){
     return new BCryptPasswordEncoder();
    }
 
 
 public static void main(String args[]) {
	 
	 String password = "pass123";
	 
	 BCryptPasswordEncoder ab = new BCryptPasswordEncoder();
	 String com = ab.encode(password);
	 System.out.println(com);
	 
	 boolean result = ab.matches(password, com);
	 
	 System.out.println(result);
 }
}
package dccash.money.web.corporate.controller;

import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
public class Index_Controller {
	
    @Autowired
    DataSource dataSource;
	
	@RequestMapping("/index")
	public String Salesprofile() {
		 return "corporate/index";
	}
	
/*    @RequestMapping("/corporate_main")
    public String hello(Model model, @RequestParam(value="name", required=false, defaultValue="World 2") String name) {
        model.addAttribute("name", name);
        return "index";
    }*/

}
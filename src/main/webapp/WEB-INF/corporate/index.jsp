<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Daily Cashe</title>
<meta content="width=device-width, initial-scale=1.0" name="viewport">
<meta content="" name="keywords">
<meta content="" name="description">

<!-- Favicons -->
<link
	href="${pageContext.request.contextPath}/corporate/img/dc/logo.png"
	rel="icon">
<link
	href="${pageContext.request.contextPath}/corporate/img/apple-touch-icon.png"
	rel="apple-touch-icon">

<!-- Google Fonts -->
<link
	href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Raleway:300,400,500,700,800|Montserrat:300,400,700"
	rel="stylesheet">

<!-- Bootstrap CSS File -->
<link
	href="${pageContext.request.contextPath}/corporate/lib/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">

<!-- Libraries CSS Files -->
<link
	href="${pageContext.request.contextPath}/corporate/lib/font-awesome/css/font-awesome.min.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/corporate/lib/animate/animate.min.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/corporate/lib/ionicons/css/ionicons.min.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/corporate/lib/owlcarousel/assets/owl.carousel.min.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/corporate/lib/magnific-popup/magnific-popup.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/corporate/lib/ionicons/css/ionicons.min.css"
	rel="stylesheet">

<!-- Main Stylesheet File -->
<link href="${pageContext.request.contextPath}/corporate/css/style.css"
	rel="stylesheet">

<!-- =======================================================
    Theme Name: Reveal
    Theme URL: https://bootstrapmade.com/reveal-bootstrap-corporate-template/
    Author: BootstrapMade.com
    License: https://bootstrapmade.com/license/
  ======================================================= -->

<style>
.nav-menu li:hover>a, .nav-menu>.menu-active>a {
	color: #FFFF00;
}

.bq {
	font-family: "proxima-nova-soft", sans-serif;
	font-size: 20px;
	font-weight: 400;
	line-height: 1.5;
	text-align: center;
}

#intro .intro-content .btn-get-started {
	color: #ffff00;
	background: none;
    border: 2px solid #ffff00;
}

#intro .intro-content .btn-get-started:hover {
  color: #ffffff;
  border: 2px solid #ffffff;
}
</style>
</head>

<body id="body">

  <!--==========================
    Top Bar
  ============================-->
  <section id="topbar" class="d-none d-lg-block">
    <div class="container clearfix">
      <div class="contact-info float-left">
        <i class="fa fa-envelope-o"></i> <a href="mailto:contact@example.com">support@dccash.money</a>
        <i class="fa fa-phone"></i> +6019 322 3599
      </div>
      <div class="social-links float-right">
        <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
      </div>
    </div>
  </section>

  <!--==========================
    Header
  ============================-->
  <header id="header">
    <div class="container">

      <div id="logo" class="pull-left">
        <h1><a href="#body" class="scrollto"><img src="${pageContext.request.contextPath}/corporate/img/dc/logo.png" alt="" title="" style="width:35px;height:auto;" /><span><b style="font-size:0.6em;color:white"> Daily Cashe</b></span></a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <%-- <h1><a href="#body"><img src="${pageContext.request.contextPath}/corporate/img/dc/logo.png" alt="" title="" style="width:auto;height:auto" /></a></h1> --%>
      </div>

      <nav id="nav-menu-container">
        <ul class="nav-menu">
          <li class="menu-active"><a href="#body">Home</a></li>
          <li><a href="#about">DC Wallet</a></li>
          <li><a href="#services">Services</a></li>
          <li><a href="#portfolio">Sharing</a></li>
          <!-- <li><a href="#team">Team</a></li> -->
          <li><a href="#contact">Contact</a></li>
        </ul>
      </nav><!-- #nav-menu-container -->
    </div>
  </header><!-- #header -->

	
  <!--==========================
    Intro Section
  ============================-->
  <section id="intro">
    <%-- <div class="intro-content">
      <br>
      <h2 style="font-size:1em;">Need money? <span style="color:black;">Say YES to DC</span><br> DC bring money, money bring power, power bring fame, fame change your life<br><br><div style="color:black;">For our collective future - <b>"DC Wallet"</b></div></b></h2>
      <div>
      	<a><img src="${pageContext.request.contextPath}/corporate/img/dc/youtube.png" style="height:80px;width:auto;" alt="" data-toggle="modal" data-target="#myModal"></a>
        <a href="${pageContext.request.contextPath}/member" class="btn-get-started scrollto">Login</a>
        <!-- <a href="#services" class="btn-projects scrollto">Our Incubator</a> -->
      </div>
    </div> --%>
    
    <div class="intro-content">
      <br>
      <h2 style="color:white;">Happy Employees</h2>
      <h2 style="color:white;">Stay Longer</h2>
      <div>
      	<%-- <a><img src="${pageContext.request.contextPath}/corporate/img/dc/youtube.png" style="height:80px;width:auto;" alt="" data-toggle="modal" data-target="#myModal"></a>--%>
      	<a href="#call-to-action" class="btn-get-started scrollto">EMPLOYEES</a><br>
        <a href="${pageContext.request.contextPath}/member" class="btn-get-started scrollto">EMPLOYERS</a>
        <!-- <a href="#services" class="btn-projects scrollto">Our Incubator</a> -->
      </div>
    </div> 
    
    <div id="intro-carousel" class="owl-carousel" >
      <div class="item" style="background-image: url('${pageContext.request.contextPath}/corporate/img/intro-carousel/1.jpg');"></div>
      <div class="item" style="background-image: url('${pageContext.request.contextPath}/corporate/img/intro-carousel/2.jpg');"></div>
      <div class="item" style="background-image: url('${pageContext.request.contextPath}/corporate/img/intro-carousel/3.jpg');"></div>
      <div class="item" style="background-image: url('${pageContext.request.contextPath}/corporate/img/intro-carousel/4.jpg');"></div>
      <div class="item" style="background-image: url('${pageContext.request.contextPath}/corporate/img/intro-carousel/5.jpg');"></div>
    </div>

  </section><!-- #intro -->

  <main id="main">
  
  <!--==========================
      About Section
    ============================-->
    <section id="about1" class="wow fadeInUp" >
      <div class="container">
          <br><br>
        
          <%-- <div class="col-lg-6 about-img">
            <img src="${pageContext.request.contextPath}/corporate/img/dc/DC_phone.png" alt="">
          </div> --%>

          <div class="col-lg-12 content" style="font-size:0.9em;">
            <p class="bq">
				A unique mobile app that specially designed to assist you on tight financing. 
				Through the mobile app, we can solve your issues by bringing you a
				better service throughout the seamless process of accessing Daily Cash and enjoying the additional
				services that you will need. 				
			</p>
			<p style="text-align:right"><b> - Founder : John </b></p>
           
          </div>
        

      </div>
    </section><!-- #about --> 
    <hr>

    <section id="about" class="wow fadeInUp" >
      <div class="container">
      	<header class="section-header">
          <h3 style="text-align:center">Your company cares deeply about your financial well-being</h3>
        </header>
        <div class="row">
        
          <div class="col-lg-6 about-img">
            <img src="${pageContext.request.contextPath}/corporate/img/dc/DC_phone.png" alt="">
          </div>

          <div class="col-lg-6 content" style="font-size:0.9em;">
          <br>
            <h3 style="font-size:1.6em;"><b>DC Wallet</b></h3>
            <h3>Your Problem, Our Responsibility</h3>
            <ul>
              <li><i class="ion-android-checkmark-circle"></i> Where Communities Unite & Prosper Together.</li>
              <li><i class="ion-android-checkmark-circle"></i> Not for profit. For people.</li>
              <li><i class="ion-android-checkmark-circle"></i> When money is the problem - we're the solution.</li>
              <li><i class="ion-android-checkmark-circle"></i> You're Not Alone, If You Need advance cash.</li>
              <li><i class="ion-android-checkmark-circle"></i> Payment without cash- It’s Possible.</li>
              <li><i class="ion-android-checkmark-circle"></i> Anyone can save money.</li>     
            </ul>
           
          </div>
        </div>

      </div>
    </section><!-- #about -->
    
    <section id="about" class="wow fadeInUp" >
      <div class="container">
      	<header class="section-header">
          <h3 style="text-align:center">About MPay Mastercard Prepaid Card</h3>
        </header>
		<div class="col-lg-12 content" style="font-size: 0.9em;">
			<p class="bq">
				MPay Mastercard Prepaid Card is an ideal cashless
				payment solution that offers convenience and safe way to purchase
				goods and services. The card can be used at all domestic and
				worldwide retail stores, plus allowing cash withdrawal via
				Mastercard network.
			</p>
		</div>
		<div class="col-lg-12 about-img2">
			<img src="${pageContext.request.contextPath}/corporate/img/mpaymastercard.png" alt="">
		</div>
	  </div>
    </section><!-- #about -->

    <!--==========================
      Services Section
    ============================-->
    <section id="services" style="font-size:0.9em;">
      <div class="container">
        <div class="section-header" class="wow fadeInUp">
          <!-- <h2 style="font-size:1.6em;">Services</h2> -->
          <h3 style="text-align:center;">A development hub for the greatest ideas and innovations in the Digital Sharing Economy and non regulated Financial-Technology.</h3>
          <br>
          <p style="text-align:center;"><b>"DailyCash a rare and almost mythical thing - a unicorn."</b></p>
        </div>

        <div class="row">
		   <div class="col-lg-12">
            <div class="box wow fadeInLeft" data-wow-delay="0.2s">
              <div class="icon"><img src="${pageContext.request.contextPath}/corporate/img/dc/service/atm.png" style="width:80px;transition:0.5s;line-height:0;"></div>
              <h4 class="title">Cash Withdrawal</h4>
              <p class="description">Cash accessible at any ATM over 300 countries and auto currency conversion features.</p>
            </div>
          </div>

          <div class="col-lg-12">
            <div class="box wow fadeInLeft" data-wow-delay="0.2s">
              <div class="icon"><img src="${pageContext.request.contextPath}/corporate/img/dc/service/money.png" style="width:80px;transition:0.5s;line-height:0;"></div>
              <h4 class="title">Cash Advance</h4>
              <p class="description">Personal Financing up-to your desire approval limit, terms and condition apply</p>
              <br>
            </div>
          </div>
          
         <div class="col-lg-12">
            <div class="box wow fadeInLeft" data-wow-delay="0.2s">
              <div class="icon"><img src="${pageContext.request.contextPath}/corporate/img/dc/service/qrcode.png" style="width:80px;transition:0.5s;line-height:0;"></div>
              <h4 class="title">QR Payment</h4>
              <p class="description">Seamless Payment experience, accepted at all DC partnering merchant and enjoy special discount</p>
            </div>
          </div>

         <div class="col-lg-12">
            <div class="box wow fadeInLeft" data-wow-delay="0.2s">
              <div class="icon"><img src="${pageContext.request.contextPath}/corporate/img/dc/service/salary.png" style="width:80px;transition:0.5s;line-height:0;"></div>
              <h4 class="title">Payroll Management</h4>
              <p class="description">Advance Payroll Management System, hassel free from Account Opening to Daily/Monthly Salary Payout</p>
            </div>
          </div>
          
        </div>

      </div>
    </section><!-- #services -->

    <!--==========================
      Clients Section
    ============================-->
    <section id="clients" class="wow fadeInUp" style="font-size:0.9em;">
      <div class="container">
        <div class="section-header">
          <!-- <h2 style="font-size:1.6em;">DC Partners</h2> -->
          <h3 style="text-align:center;">Partners across Malaysia and regional ASEAN</h3>
        </div>
        
        <div class="row contact-info">

          <div class="col-md-6">
            <div class="contact-email">
              <img class="img-responsive center-block" src="${pageContext.request.contextPath}/corporate/img/dc/partner/p3.png" style="height:100px;width:auto;display:block;margin-left:auto;margin-right:auto;" alt="">
            </div>
          </div>
          
          <div class="col-md-6">
            <div class="contact-email">
              <img class="img-responsive center-block" src="${pageContext.request.contextPath}/corporate/img/dc/partner/p4.png" style="height:100px;width:auto;display:block;margin-left:auto;margin-right:auto;" alt="">
           </div>
          </div>
          
          <div class="col-md-6">
            <div class="contact-address">
              <img class="img-responsive center-block" src="${pageContext.request.contextPath}/corporate/img/dc/partner/p5.png" style="height:100px;width:auto;display:block;margin-left:auto;margin-right:auto;" alt="">
        	</div>
          </div>

          <div class="col-md-6">
            <div class="contact-phone">
              <img class="img-responsive center-block" src="${pageContext.request.contextPath}/corporate/img/dc/partner/p2.png" style="height:100px;width:auto;display:block;margin-left:auto;margin-right:auto;" alt="">          
            </div>
          </div>
          
          <div class="col-md-12">
            <div class="contact-email">
              <img class="img-responsive center-block" src="${pageContext.request.contextPath}/corporate/img/dc/partner/p1.png" style="height:100px;width:auto;display:block;margin-left:auto;margin-right:auto;" alt="">
            </div>
          </div>

        </div>

		
        <%-- <div class="owl-carousel clients-carousel">
          <img class="img-responsive center-block" src="${pageContext.request.contextPath}/corporate/img/dc/partner/p1.png" style="height:100px;width:auto;display:block;margin-left:auto;margin-right:auto;" alt="">
          <img class="img-responsive center-block" src="${pageContext.request.contextPath}/corporate/img/dc/partner/p2.png" style="height:100px;width:auto;display:block;margin-left:auto;margin-right:auto;" alt="">
          <img class="img-responsive center-block" src="${pageContext.request.contextPath}/corporate/img/dc/partner/p3.png" style="height:100px;width:auto;display:block;margin-left:auto;margin-right:auto;" alt="">
          <img class="img-responsive center-block" src="${pageContext.request.contextPath}/corporate/img/dc/partner/p4.png" style="height:100px;width:auto;display:block;margin-left:auto;margin-right:auto;" alt="">
          <img class="img-responsive center-block" src="${pageContext.request.contextPath}/corporate/img/dc/partner/p5.png" style="height:100px;width:auto;display:block;margin-left:auto;margin-right:auto;" alt="">
        </div> --%>

      </div>
    </section><!-- #clients -->

    <!--==========================
      Testimonials Section
    ============================-->
     <section id="testimonials" class="wow fadeInUp">
      <div class="container">
        <div class="section-header">
          <!-- <h3 style="text-align:center">Testimonials</h3> -->
          <h3 style="text-align:center">See how our partners and their employees have saved</h3>
          <!-- <p>Lets hear what DC users topic on</p> -->
        </div>
        <div class="owl-carousel testimonials-carousel">

            <div class="testimonial-item">
              <p>
                <img src="${pageContext.request.contextPath}/corporate/img/quote-sign-left.png" class="quote-sign-left" alt="">
                Its a beautiful app in this new era. Transaction should be digitalized and slowly do away with cash transactions. This will in return improve transaction transparency, it is a safer method to carry our money, and ultimately reduce wastage caused by cash handling. Im loving this app so much.
                <img src="${pageContext.request.contextPath}/corporate/img/quote-sign-right.png" class="quote-sign-right" alt="">
              </p>
              <img src="${pageContext.request.contextPath}/corporate/img/testimonial-2.jpg" class="testimonial-img" alt="">
              <h3>Sara Wilsson</h3>
            </div>

            <div class="testimonial-item">
              <p>
                <img src="${pageContext.request.contextPath}/corporate/img/quote-sign-left.png" class="quote-sign-left" alt="">
                Yes it can be used almost like android pay or wechat! And more. Besides paying in-store, you can pay your bills, do mobile phone top up, borrow and even donate money with our app. <br><br><br>
                <img src="${pageContext.request.contextPath}/corporate/img/quote-sign-right.png" class="quote-sign-right" alt="">
              </p>
              <img src="${pageContext.request.contextPath}/corporate/img/testimonial-4.jpg" class="testimonial-img" alt="">
              <h3>Matt Brandon</h3>
            </div>
            
           <div class="testimonial-item">
              <p>
                <img src="${pageContext.request.contextPath}/corporate/img/quote-sign-left.png" class="quote-sign-left" alt="">
                I like DC very well. I use it sell topup around my friends, so I can get the reward from it. Love the Advance Cash withdrawal, it solve my financial problem when I need it. <br><br><br><br>
                <img src="${pageContext.request.contextPath}/corporate/img/quote-sign-right.png" class="quote-sign-right" alt="">
              </p>
              <img src="${pageContext.request.contextPath}/corporate/img/testimonial-5.jpg" class="testimonial-img" alt="">
              <h3>Richard Small</h3>
            </div>

        </div>

      </div>
    </section>
    
    <!--==========================
      Our Portfolio Section
    ============================-->
	<section id="portfolio" class="wow fadeInUp" style="font-size:0.9em;">
      <div class="container">
        <div class="section-header">
          <h3 style="text-align:center;">Learn about how DC Wallet work</h3>
          <!-- <p>Digital Economy sharing gallery</p> -->
        </div>

        <div class="row contact-info">

          <div class="col-md-6">
            <div class="contact-address">           
              <embed src="https://www.youtube.com/embed/sahN-4nk8LM" alt="">
            </div>
          </div>

          <div class="col-md-6">
            <div class="contact-email">            
              <embed src="https://www.youtube.com/embed/Pqf4jPXcBq0" alt=""> 
            </div>
          </div>
        </div>
      </div>
    </section>

    <!--==========================
      Our Team Section
    ============================-->
    <!-- <section id="team" class="wow fadeInUp" style="font-size:0.9em;">
      <div class="container">
        <div class="section-header">
          <h2 style="font-size:1.6em;">Our Team/精英团队</h2>
        </div>
        <div class="row">
          <div class="col-lg-3 col-md-6">
            <div class="member">
              <div class="pic"><img src="" alt=""></div>
              <div class="details">
                <h4>Lim Kee Siang</h4>
                <span>Founder</span>
					<div class="social">
	                  <a href=""><i class="fa fa-twitter"></i></a>
	                  <a href=""><i class="fa fa-facebook"></i></a>
	                  <a href=""><i class="fa fa-google-plus"></i></a>
	                  <a href=""><i class="fa fa-linkedin"></i></a>
	                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-6">
            <div class="member">
              <div class="pic"><img src="" alt=""></div>
              <div class="details">
                <h4>Alvin Phang</h4>
                <span>Founder</span>
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-6">
            <div class="member">
              <div class="pic"><img src="" alt=""></div>
              <div class="details">
                <h4>Aron</h4>
                <span>CEO</span>
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-6">
            <div class="member">
              <div class="pic"><img src="" alt=""></div>
              <div class="details">
                <h4>Crystal Lee</h4>
                <span>COO</span>
              </div>
            </div>
          </div>
        </div>

      </div>
    </section> --><!-- #team -->

    <!--==========================
      Contact Section
    ============================-->
    <section id="contact" class="wow fadeInUp" style="font-size:0.9em;">
      <div class="container">
        <div class="section-header">
          <h3 style="font-size:1.6em;">Contact Us</h3>
          <p>If you require any further information, feel free to contact us.</p>
        </div>

        <div class="row contact-info">
          <div class="col-md-4">
            <div class="contact-address">
              <i class="ion-ios-location-outline"></i>
              <h3>Address</h3>
              <address>Jalan Subang Mas, Taman Subang Mas, 47620 Subang Jaya, Selangor</address>
            </div>
          </div>

          <div class="col-md-4">
            <div class="contact-phone">
              <i class="ion-ios-telephone-outline"></i>
              <h3>Phone Number</h3>
              <p><a href="tel:+155895548855">+6019 322 3599</a></p>
            </div>
          </div>

          <div class="col-md-4">
            <div class="contact-email">
              <i class="ion-ios-email-outline"></i>
              <h3>Email</h3>
              <p><a href="mailto:info@example.com">support@dccash.money</a></p>
            </div>
          </div>

        </div>
      

      <div id="google-map" data-latitude="3.0704885" data-longitude="101.5566009"></div></div>

<!--       <div class="container">
        <div class="form">
          <div id="sendmessage">Your message has been sent. Thank you!</div>
          <div id="errormessage"></div>
          <form action="" method="post" role="form" class="contactForm">
            <div class="form-row">
              <div class="form-group col-md-6">
                <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                <div class="validation"></div>
              </div>
              <div class="form-group col-md-6">
                <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
                <div class="validation"></div>
              </div>
            </div>
            <div class="form-group">
              <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
              <div class="validation"></div>
            </div>
            <div class="form-group">
              <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
              <div class="validation"></div>
            </div>
            <div class="text-center"><button type="submit">Send Message</button></div>
          </form>
        </div>
      </div> -->
      
    </section><!-- #contact -->
    
     <!--==========================
      Call To Action Section
    ============================-->
    <section id="call-to-action" class="wow fadeInUp" style="font-size:0.9em;">
      <div class="container">
      	<div class="row">
      		<div class="col-lg-12 text-center text-lg-left">
            <h3 class="cta-title">DC Showroom</h3>
            <p class="cta-text">Bleeding-edge technologies and the latest business strategies are coming fast. Some are evolving at a rapid pace, some will take longer to grab hold. But all will have an impact on the way we work and on our economy, culture, environment, health and well-being.</p>
          </div>
      	</div>
      
        <div class="row">
		  <div class="col-lg-12 text-center text-lg-left">
            <h4 class="cta-title">Available At</h4>
          </div>
          <div class="col-lg-12 text-center" >
            <a class="cta-btn text-lg-left" href="#" style="width:100%;"><img src="${pageContext.request.contextPath}/corporate/img/dc/googledl.png" style="width:300px;transition:0.5s;"></a>
          </div>
        </div>
      </div>
    </section><!-- #call-to-action -->

  </main>
  
  

  <!--==========================
    Footer
  ============================-->
  <footer id="footer">
    <div class="container">
      <div class="copyright" style="color:white;">
        &copy; Copyright <strong>DC Wallet</strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!--
          All the links in the footer should remain intact.
          You can delete the links only if you purchased the pro version.
          Licensing information: https://bootstrapmade.com/license/
          Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Reveal
        -->
        Designed by 
        <br>
        <span style="color:white;">Essential USD Technology "Partner of DC"</span>
      </div>
    </div>
  </footer><!-- #footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <!-- JavaScript Libraries -->
  <script src="${pageContext.request.contextPath}/corporate/lib/jquery/jquery.min.js"></script>
  <script src="${pageContext.request.contextPath}/corporate/lib/jquery/jquery-migrate.min.js"></script>
  <script src="${pageContext.request.contextPath}/corporate/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="${pageContext.request.contextPath}/corporate/lib/easing/easing.min.js"></script>
  <script src="${pageContext.request.contextPath}/corporate/lib/superfish/hoverIntent.js"></script>
  <script src="${pageContext.request.contextPath}/corporate/lib/superfish/superfish.min.js"></script>
  <script src="${pageContext.request.contextPath}/corporate/lib/wow/wow.min.js"></script>
  <script src="${pageContext.request.contextPath}/corporate/lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="${pageContext.request.contextPath}/corporate/lib/magnific-popup/magnific-popup.min.js"></script>
  <script src="${pageContext.request.contextPath}/corporate/lib/sticky/sticky.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCCAR-izbwYYj390Hk_xwGZlYPGb8KfOiQ&callback=initMap"></script>
  <!-- Contact Form JavaScript File -->
  <script src="${pageContext.request.contextPath}/corporate/contactform/contactform.js"></script>

  <!-- Template Main Javascript File -->
  <script src="${pageContext.request.contextPath}/corporate/js/main.js"></script>

</body>

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
<!--         <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div> -->
        <div class="modal-body">
          <embed width="100%" height="400px" src="https://www.youtube.com/embed/sahN-4nk8LM" alt="" >
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
</html>
